package vti.com.entity;

import java.util.Date;

public class Account {
    private static int count;
    private static int accountId;
    public String email;
    public String userName;
    private String fullName;
    public Date createDate;

    public Account(){
        this.accountId = count++;
    }
    public int getAccountId(){
        return this.accountId;
    }
    public void setAccountId(int accountId){
        this.accountId = accountId;
    }
    public String getEmail(){
        return email;
    }
    public void setEmail(String email){
        this.email = email;
    }



}
