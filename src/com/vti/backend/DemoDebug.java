package com.vti.backend;

public class DemoDebug {
    public static void main(String[] args) {
        String s1 = "abc";
        String s2 = new String("abc");

        boolean check1 = (s1 == s2);
        boolean check2 = s1.equals(s2);
        System.out.println(check1);
        System.out.println(check2);
        }
    }

