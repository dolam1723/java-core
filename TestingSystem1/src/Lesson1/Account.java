package Lesson1;

import java.time.LocalDate;
import java.util.Date;

public class Account {
    int accountId;
    String email;
    String userName;
    String fullName;
    Department department;
    Position position;
    LocalDate createDate;
    Group[] groups;
}
