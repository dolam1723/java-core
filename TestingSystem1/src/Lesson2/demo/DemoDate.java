package Lesson2.demo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;

public class DemoDate {
    public static void main(String[] args) {
        Date date = new Date();
        System.out.println(date);

        // pattern
        DateFormat dateFormat = new SimpleDateFormat("dd-M-YYYY H:mm:ss");
        String stringDate = dateFormat.format(date);
        System.out.println(stringDate);

        // theo khu vực
        Locale locale = new Locale("vi", "VN");
        DateFormat dateFormat1 = DateFormat.getDateInstance(DateFormat.DEFAULT, locale);
        String stringDate2 = dateFormat1.format(date);
        System.out.println(stringDate2);
    }
}
