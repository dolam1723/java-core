package Lesson2.demo;

import java.time.LocalDate;
import java.util.Random;

public class RandomDate {
    public static void main(String[] args) {
        Random random = new Random();
        int minDate = (int) LocalDate.of(2010,1,1).toEpochDay();
        System.out.println(minDate);
        int maxDate = (int) LocalDate.now().toEpochDay();
        long randomLong = random.nextInt(maxDate - minDate);

        LocalDate randomDate =  LocalDate.ofEpochDay(randomLong);
        System.out.println(randomDate);
    }
}
