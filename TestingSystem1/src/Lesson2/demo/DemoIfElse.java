package Lesson2.demo;

public class DemoIfElse {
    public static void main(String[] args) {
        int x = 10;
        int y = 20;

        if (x == y) {
            System.out.println("x bang y");
        } else if (x < y) {
            System.out.println("x nho hon y");
        } else if (x > y) {
            System.out.println("x lon hon y");
        } else System.out.println("cac truong hop khac");

        String text = "";
        text = (x>y) ? "x la so lon hon" : "cac truong hop con lai";
        System.out.println(text);


    }
}
