package Lesson2.demo;

public class DemoWhile {
    public static void main(String[] args) {
        System.out.println("Demo While");
        int i = 0;
        while (i < 5){
            System.out.println(i);
            i++;
        }
        System.out.println("Demo DoWhile");
        int abc = 0;
        do {
            abc ++;
            System.out.println(abc);
        } while (abc < 5);
    }
}
