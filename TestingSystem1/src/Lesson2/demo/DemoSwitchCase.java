package Lesson2.demo;

public class DemoSwitchCase {
    public static void main(String[] args) {
        int number = 2;
        switch (number) {
            case 2:
                System.out.println("lua chon so 2");
                break;
            case 1:
                System.out.println("lua chon so 1");
                break;
            case 3:
                System.out.println("lua chon so 3");
                break;
            default:
                System.out.println("khong co lua chon");
                break;
        }
        if ( number == 1){
            System.out.println("lua chon so 1");
        } else if ( number == 2){
            System.out.println("lua chon so 2");
        } else if ( number == 3){
            System.out.println("lua chon so 3");
        }
    }
}
