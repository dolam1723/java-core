package Lesson2.assignment2;

import java.time.LocalDate;
import java.util.Random;
import java.util.Scanner;

public class Assignment2 {
    public static void main(String[] args) {
        // --------------------------exercise 4: Random number --------------------------
        System.out.println("Exercise 4: Random number");
        System.out.println("-------------Question 1-------------");
        Random random = new Random();
        int number = random.nextInt();
        System.out.println("So bat ky` " + number);

        System.out.println("-------------Question 2-------------");
        float number2 = random.nextFloat();
        System.out.println("So thuc bat ky` " + number2);

        System.out.println("-------------Question 3-------------");
        String[] arrName = {"Nguyễn Văn A", "Nguyễn Văn B", "Đỗ Văn C"};
        int numberRandom = random.nextInt(2);
        String name = arrName[numberRandom];
        System.out.println(name);

        System.out.println("-------------Question 4-------------");
        int minDate = (int) LocalDate.of(1995, 07, 24).toEpochDay();
        int maxDate = (int) LocalDate.of(1995, 12, 20).toEpochDay();
        long randomLong = minDate + random.nextInt(maxDate - minDate);
        LocalDate localDate = LocalDate.ofEpochDay(randomLong);
        System.out.println(localDate);
        System.out.println("--------------Question 5: Lấy ngẫu nhiên 1 ngày ");

        int now = (int) LocalDate.now().toEpochDay();
        int randomDate = now - random.nextInt(365);
        LocalDate reusultDate = LocalDate.ofEpochDay(randomDate);
        System.out.println("Ngày ngẫu nhiên là: " + reusultDate);

        System.out.println("-----------Question 6: Lấy ngẫu nhiên 1 ngày trong quá khứ------------");

        int maxDay1 = (int) LocalDate.now().toEpochDay();
        long randomDay1 = random.nextInt(maxDay1);
        LocalDate resultDate1 = LocalDate.ofEpochDay(randomDay1);
        System.out.println("1 Ngày ngẫu nhiên trong quá khứ: " + resultDate1);

        System.out.println("-----------Question 7: Lấy ngẫu nhiên 1 số có 3 chữ số----------");
        int z = random.nextInt(999 - 100 + 1) + 100;
        System.out.println(z);


        System.out.println("Exercise 5: Input from console");
        System.out.println("------------question 1------------");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mời bạn nhập vào 3 số nguyên");
        System.out.println("Nhập vào số 1: ");
        int a = scanner.nextInt();
        System.out.println("Nhập vào số 2: ");
        int b = scanner.nextInt();
        System.out.println("Nhập vào số 3: ");
        int c = scanner.nextInt();
        System.out.println("Bạn vừa nhập vào các số: " + a + " " + b + " " + c);

        System.out.println("---------------Question 2------------------");
        Scanner scanner1 = new Scanner(System.in);
        System.out.println("Mời bạn nhập vào 2 số thực");
        System.out.println("Nhập vào số 1: ");
        float f1 = scanner1.nextFloat();
        System.out.println("Nhập vào số 2: ");
        float f2 = scanner1.nextFloat();
        System.out.println("Bạn vừa nhập vào các số: " + f1 + " " + f2);
        Scanner scanner2 = new Scanner(System.in);

        System.out.println("-------------Question 3----------------");
        System.out.println("Mời bạn nhập vào Họ: ");
        String s1 = scanner2.next();
        System.out.println("Mời bạn nhập vào Tên: ");
        String s2 = scanner2.next();
        System.out.println("Fullname của bạn là:" + s1 + " " + s2);

        Scanner scanner3 = new Scanner(System.in);
        System.out.println("-------------Question 4----------------");
        System.out.println("Mời bạn nhập vào năm sinh: ");
        int year = scanner3.nextInt();
        System.out.println("Mời bạn nhập vào tháng sinh: ");
        int month = scanner3.nextInt();
        System.out.println("Mời bạn nhập vào ngày sinh: ");
        int day = scanner3.nextInt();
        LocalDate dateBirth = LocalDate.of(year, month, day);
        System.out.println("Ngày sinh của bạn là: " + dateBirth);

    }
}
