drop database if exists finalexam2;
create database finalexam2;
use finalexam2;
drop table if exists `User`;
create table `User` (
                        id			int auto_increment primary key,
                        fullname	varchar(50),
                        email		varchar(50) not null,
                        password		varchar(50) not null,
                        exp          int,
                        proskill    varchar(50),
                        projectid   int,
                        role         enum('MANAGER', 'EMPLOYEE','ADMIN')
);


INSERT INTO finalexam2.User (id, fullname, email, password, exp, proskill, projectid, role) VALUES (1, 'Manager', 'a@gmail.com', '123456', 2, null, 1, 'MANAGER');
INSERT INTO finalexam2.User (id, fullname, email, password, exp, proskill, projectid, role) VALUES (2, 'Employee', 'b@gmail.com', '123456', null, 'Java', 2, 'EMPLOYEE');
INSERT INTO finalexam2.User (id, fullname, email, password, exp, proskill, projectid, role) VALUES (3, 'Admin', 'c@gmail.com', '123456', null, null, 1, 'ADMIN');