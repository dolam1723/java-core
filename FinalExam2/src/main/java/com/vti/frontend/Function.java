package com.vti.frontend;

import com.vti.backend.controller.UserController;
import com.vti.entity.User;
import com.vti.utils.ScannerUtils;

import java.util.List;

public class Function {
    UserController controller = new UserController();
    public User login(){
        System.out.println("Email: ");
        String email = ScannerUtils.inputEmail();
        System.out.println("Password: ");
        String password = ScannerUtils.inputPassword();
        return controller.login(email,password);
    }
    public void findAllUserByProjectId(){
        System.out.println("Nhap Id project can tim: ");
        int projectId = ScannerUtils.inputNumber();
        List<User> userList = controller.findAllUserByProjectId(projectId);
        System.out.println("Danh sach user project " + projectId);
        String leftAlignFormat = "| %-4s | %-20s | %-20s |%n";
        System.out.format("+------+----------------------+----------------------+%n");
        System.out.format("|  id  |       fullname       |         email        |%n");
        System.out.format("+------+----------------------+----------------------+%n");
        for (User user : userList){
            System.out.format(leftAlignFormat,user.getId(),user.getFullName(),user.getEmail());
        }
        System.out.format("+------+----------------------+----------------------+%n");
    }
    public void creatUser(){
        System.out.println("----- them moi user-----");
        System.out.println("moi b nhap full name");
        String fullName = ScannerUtils.inputString();
        System.out.println("moi b nhap email");
        String email = ScannerUtils.inputEmail();
        controller.creatUser(fullName, email);
    }
}
