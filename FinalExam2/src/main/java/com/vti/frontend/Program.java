package com.vti.frontend;

import com.vti.entity.User;
import com.vti.utils.ScannerUtils;

public class Program {
    public static void main(String[] args) {
        Function function = new Function();
        System.out.println("Login");
        while (true){
            User user = function.login();
            if (user == null) {
                System.out.println("Username or password that you've entered is incorrect");
            } else {
                System.out.println("Welcome "+ user.getFullName());
                menu();
            }
        }
    }
    public static void menu(){
        Function function = new Function();
        while (true){
            System.out.println("Chon chuc nang");
            System.out.println("1. In ra tat ca employee va manager bang Id project");
            System.out.println("2. Tao user");
            System.out.println("3. Thoat");
            int choose = ScannerUtils.inputNumber(1,3);
            switch (choose){
                case 1:
                    function.findAllUserByProjectId();
                    break;
                case 2:
                    function.creatUser();
                    break;
                case 3:
                    return;
            }
        }
    }
}
