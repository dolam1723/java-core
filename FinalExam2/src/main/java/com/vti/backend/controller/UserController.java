package com.vti.backend.controller;

import com.vti.backend.service.UserService;
import com.vti.entity.User;

import java.util.List;

public class UserController {
    UserService service = new UserService();

    public List<User> findAllUserByProjectId(int projectId) {
        return service.findAllUserByProjectId(projectId);
    }

    public User login(String email, String password) {
        return service.login(email, password);
    }

    public void creatUser(String fullName, String email) {
        service.creatUser(fullName, email);
    }
}
