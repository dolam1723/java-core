package com.vti.backend.service;

import com.vti.backend.repository.UserRepository;
import com.vti.entity.User;

import java.util.List;

public class UserService implements IUserService{
    UserRepository repository = new UserRepository();

    @Override
    public List<User> findAllUserByProjectId(int projectId) {
        return repository.findAllUserByProjectId(projectId);
    }

    @Override
    public User login(String email, String password) {
        return repository.login(email, password);
    }

    @Override
    public void creatUser(String fullName, String email) {
        repository.creatUser(fullName, email);
    }
}
