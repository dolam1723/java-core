package com.vti.backend.repository;

import com.vti.entity.Role;
import com.vti.entity.User;
import com.vti.utils.JdbcUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserRepository {
    public User login(String email, String password){
        String sql = "SELECT * FROM finalexam2.user WHERE email = ? AND password = ?";
        Connection connection = JdbcUtils.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,email);
            preparedStatement.setString(2,password);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setFullName(resultSet.getString("fullname"));
                user.setEmail(email);
                user.setProjectId(resultSet.getInt("projectid"));
                return user;
            }
        } catch (SQLException throwables) {
            System.err.println(throwables.getMessage());
        }
        return null;
    }
    public List<User> findAllUserByProjectId(int projectId){
        List<User> userList = new ArrayList<>();
        String sql = "SELECT * FROM finalexam2.user WHERE projectid = ? and role ='employee'  or role ='manager' ";
        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, projectId);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setFullName(resultSet.getString("fullname"));
                user.setEmail(resultSet.getString("email"));
                user.setRole(Role.valueOf(resultSet.getString("role")));

                userList.add(user);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        }
        return userList;
    }

    public void creatUser(String fullName, String email){
        String sql = "INSERT INTO finalexam2.user (fullname, email, password, role) VALUES (? , ?, ?, ?);";
        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, fullName);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, "123456");
            preparedStatement.setString(4, Role.EMPLOYEE.name());

            int rs = preparedStatement.executeUpdate();
            if (rs > 0){
                System.out.println("them moi " + rs + " User thanh cong!");
            } else {
                System.err.println("them moi khong thanh cong");
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        }
    }
}
