package vti.com.backend;

import vti.com.entity.Department;

import java.util.Scanner;

public class Ex2 {
    Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        Ex2 ex2 = new Ex2();
//        ex2.question1();
//        ex2.question3();
//        ex2.question4();
        System.out.println(ex2.inputAge());

    }

    public void question1() {
        try {
            float result = divide(7, 0);
            System.out.println(result);
        } catch (ArithmeticException ex) {
            System.out.println("Cannot divide 0");
        } finally {
            System.out.println("divide complete");
        }
    }

    public static float divide(int a, int b) {
        return a / b;
    }

    public void question3() {
        int[] numbers = {1, 2, 3};
        try {
            System.out.println(numbers[10]);
        } catch (ArrayIndexOutOfBoundsException exception) {
            System.out.println("Loi ArrayIndexOutOfBoundsException");
        }
    }

    public void question4() {
        Department department1 = new Department("Department1");
        Department department2 = new Department("Department2");
        Department department3 = new Department("Department3");
        Department[] Arrs = {department1, department2, department3};
        try {
            System.out.println(Arrs[10]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Cannot find department");
        }

    }

    public int inputAge() {
        System.out.println("Nhap tuoi: ");
        int Age;
        try {


            Age = scanner.nextInt();




            return Age;
        } catch (Exception ex){
            while (Age != (int) Age) {

                if (Age != (int) Age) {

                    System.out.println("wrong input! Please input an age as int");
                    Age = scanner.nextInt();
                } else if (Age < 0) {
                    System.out.println("Wrong input! The age must be greater than 0");
                    Age = scanner.nextInt();
                }
            }
        }
    }
}


