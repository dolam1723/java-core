package vti.com.entity;

public class Department {
    public int Count;
    private int departmentId;
    private String departmentName;
    public Department(String name){
        super();
        Count++;
        this.departmentId = Count;
        this.departmentName = name;
    }
    public String toString() {
        return "id=" + departmentId + "\n name=" + departmentName + "]";
    }
}
