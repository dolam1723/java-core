package main.java.Demo;

public class ABC {
    public static void main(String[] args) {
        String a = "123";
        String b = "123";
        String c = new String("123");
        String d = new String("123");

//        String ab = new StringBuilder(a).reverse().toString();
//       System.out.println(ab);
        System.out.println(a == b);
        System.out.println(a == c);
        System.out.println(a.equals(b));
        System.out.println(a.equals(c));
        System.out.println(c == d);
        System.out.println(c.equals(d));
    }
}
