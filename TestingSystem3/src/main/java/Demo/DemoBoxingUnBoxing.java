package main.java.Demo;

public class DemoBoxingUnBoxing {
    public static void main(String[] args) {
        int a =100;
        Integer aObject = Integer.valueOf(a); // boxing


        Integer b = new Integer(5);
        int bPrimitive = b.intValue(); //unboxing
    }
}
