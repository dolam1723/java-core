package main.java.assignment;

public class Exercise3 {
    public static void main(String[] args) {
        Exercise3 exercise3 = new Exercise3();
        exercise3.question1();
        exercise3.question2();
    }
    public void question1(){
        Integer luong = new Integer(5000);
        float luongConvert = luong.floatValue();
        System.out.println(luongConvert);
    }
    public void question2(){
        String ababc = "1234567";
        int convert = Integer.valueOf(ababc);
        int convert2 = Integer.parseInt(ababc);
        int convert3 = new Integer(ababc);
        System.out.println(convert);
        System.out.println(convert2);
        System.out.println(convert3);
    }
    public void question3(){
        Integer integer = new Integer(12345);
        int convert = integer.intValue();
    }

}
