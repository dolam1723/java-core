package main.java.assignment;

import java.util.Locale;
import java.util.Scanner;

public class Exercise4 {
    Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Exercise4 exercise4 = new Exercise4();
        //exercise4.question1();
        //exercise4.question2();
        //exercise4.question3();
        exercise4.question4();
    }

    public void question1() {
        System.out.println("Moi b nhap vao 1 chuoi~ : ");
        String input = scanner.nextLine();
        int length = input.length();
        System.out.println("So ky tu la: " + input.length());
    }

    public void question2() {
        System.out.println("Moi b nhap vao chuoi thu 1: ");
        String input1 = scanner.nextLine();
        System.out.println("Moi b nhap vao chuoi thu 2: ");
        String input2 = scanner.nextLine();
        String rs1 = input1 + input2;
        input1 = input1.concat(input2);
        System.out.println(rs1);
        System.out.println(input1);
    }

    public void question3() {
        System.out.println("Nhap vao ten cua b: ");
        String ten = scanner.nextLine();
        String first = ten.substring(0, 1);
        ten = ten.substring(1, ten.length());
        ten = ten.toLowerCase(Locale.ROOT);
        first = first.toUpperCase();
        System.out.println(first + ten);
    }

    public void question4() {
        System.out.println("Nhap vao ten cua b: ");
        String ten1 = scanner.nextLine();
        for (int i = 0; i < ten1.length(); i++) {
            String text = String.valueOf(ten1.charAt(i)).toUpperCase(Locale.ROOT);
            System.out.println(text);
        }
    }

    public void question5() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhap ho: ");
        String firstName = scanner.nextLine();
        System.out.println("Nhap ten: ");
        String lastName = scanner.nextLine();
        System.out.println("Ho ten ban la: " + firstName.concat(lastName));
        scanner.close();
    }

    public static void question6() {
        String fullName;
        String lastName = "", middleName = "", firstName = "";
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhap ho ten b: ");
        fullName = scanner.nextLine();
        fullName = fullName.trim();
        String[] words = fullName.split(" ");
        lastName = words[0];
        firstName = words[words.length - 1];
        for (int i = 1; i <= words.length - 2; i++) {
            middleName += words[i] + " ";
        }
        System.out.println("Ho la: " + lastName);
        System.out.println("Ten dem la: " + middleName);
        System.out.println("Ten la: " + firstName);

    }
    public static void question7() {

        Scanner scanner = new Scanner(System.in);
        String fullName;
        System.out.println("Nhap ho ten b: ");
        fullName = scanner.nextLine();
        scanner.close();
        fullName = fullName.trim();
        fullName = fullName.replaceAll("\\s+", " ");
        System.out.println("test"+ fullName);
        String[] words = fullName.split(" ");
        fullName = "";
        for (String word : words) {
            String firstCharacter = word.substring(0, 1).toUpperCase();
            String leftCharacter = word.substring(1);
            word = firstCharacter + leftCharacter;
            fullName += word + " ";
        }
        System.out.println("Ho ten cua b la: " + fullName);
    }
    public static void question8() {

        String[] groupNames = { "Java with VTI", "Cách qua môn gia va", "Học Java có khó không?" };

        for (String groupName : groupNames) {
            if (groupName.contains("Java")) {
                System.out.println(groupName);
            }
        }
}
    public static void question9() {
        String[] groupNames = { "Java", "C#", "C++" };
        for (String groupName : groupNames) {
            if (groupName.equals("Java")) {
                System.out.println(groupName);
            }
        }
    }
}
