package main.java.assignment;

public class Exercise1 {
    public static void main(String[] args) {
        Exercise1 exercise1 = new Exercise1();
        exercise1.question1();
        exercise1.question2();
        exercise1.question3();
       int kq = exercise1.question4(6,5);
        System.out.println(kq);
    }

    public void question1() {
        float luong1 = 5240.5f;
        float luong2 = 10970.055f;
        int luong1int = (int) luong1;
        int luong2int = (int) luong2;
        System.out.println("Luong account 1 la " + luong1int);
        System.out.println("Luong account 2 la " + luong2int);
    }

    public void question2() {
        int random = 120;
        String numberString = String.valueOf(random);
        while ( numberString.length() < 5   ){
            numberString =  "0" + numberString ;
        }
        System.out.println(numberString);
    }
    public void question3() {
        int random = 12345;
        int kq = random % 100;
        System.out.println(kq);
    }
    public int question4(int a,int b){
        return a/b;
    }


}
