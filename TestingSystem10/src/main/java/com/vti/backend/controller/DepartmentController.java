package com.vti.backend.controller;

import com.vti.backend.service.DepartmentService;
import com.vti.entity.Department;

import java.util.List;

public class DepartmentController {
    DepartmentService departmentService = new DepartmentService();

    public List<Department> getDepartments() {
        return departmentService.getDepartments();
    }

    public List<Department> getDepartmentsById(int id) {
        return departmentService.getDepartmentsById(id);
    }

    public boolean isDepartmentNameExists(String name) {
        return departmentService.isDepartmentNameExists(name);
    }
    public boolean isDepartmentIdExists(int id){
        return departmentService.isDepartmentIdExists(id);
    }

    public void createDepartment(String name) {
        departmentService.createDepartment(name);
    }
    public void updateDepartment(int id,String oldName,String newName){
        departmentService.updateDepartment(id,oldName,newName);
    }
}
