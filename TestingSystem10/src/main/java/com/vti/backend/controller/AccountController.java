package com.vti.backend.controller;

import com.vti.backend.service.AccountService;
import com.vti.entity.Account;

import java.util.List;

public class AccountController {
    AccountService accountService = new AccountService();

    public void createAccount(String username, String email, String password) {
        accountService.createAccount(username, email, password);
    }

    public void updateAccount(int id, String oldPassword, String newPassword) {
        accountService.updateAccount(id, oldPassword, newPassword);
    }

    public void deleteAccount(int id) {
        accountService.deleteAccount(id);
    }

    public List<Account> findByEmail(String emailtim) {
        return accountService.findByEmail(emailtim);
    }

    public List<Account> getAllAccount() {
        return getAllAccount();
    }

    public boolean loginAccount(String email, String password) {
        return accountService.loginAccount(email, password);
    }
}
