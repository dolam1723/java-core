package com.vti.backend.repository;

import com.vti.entity.Account;
import com.vti.utils.JdbcUtils;
import com.vti.utils.ScannerUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AccountRepository {
    public void createAccount(String username, String email, String password) {
        // Tao 1 query -> tuong ung voi chuc nang muon
        String sql = "INSERT INTO jdbc.Account (full_name, email, password) Value(?,?,?)";
// ket noi toi DB -> tao phien lam viec
        Connection connection = JdbcUtils.getConnection();
// tao statement tuong ung voi query ( co bien : PreparedStatement, ko bien : Statement
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, password);
// excute cau query -> lay ket qua (result)
            int resultSet = preparedStatement.executeUpdate();
// kiem tra thanh cong -> thong bao
            if (resultSet == 0) {
                System.out.println("Failed");
            } else {
                System.out.println("Success");
            }
            JdbcUtils.closeConnection();
        } catch (SQLException throwables) {
            System.out.println(throwables.getMessage());
        }
    }
    public void updateAccount(int id, String oldPassword, String newPassword) {
        String sql = "UPDATE jdbc.account set password = ? where account_id = ? and password = ?";
        Connection connection = JdbcUtils.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, newPassword);
            preparedStatement.setInt(2, id);
            preparedStatement.setString(3, oldPassword);
            int rs = preparedStatement.executeUpdate();
            if (rs == 0) {
                System.out.println("Failed");
            } else {
                System.out.println("Success");
            }

        } catch (SQLException throwables) {
            System.out.println(throwables.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        }
    }
    public void deleteAccount(int id) {
        String sql = "Delete from jdbc.account where account_id = ?";
        Connection connection = JdbcUtils.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            int rs = preparedStatement.executeUpdate();
            if (rs == 0) {
                System.out.println("Failed");
            } else {
                System.out.println("Success");
            }

        } catch (SQLException throwables) {
            System.out.println(throwables.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        }
    }
    public List<Account> findByEmail(String emailtim) {
        List<Account> accounts = new ArrayList<>();
        String words = "%" + emailtim + "%";
        String sql = "select * from jdbc.account where email like ?";
        Connection connection = JdbcUtils.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, words);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Account account = new Account();
                int accountId = resultSet.getInt("account_id");
                account.setAccountId(accountId);
                account.setFullName(resultSet.getString("full_name"));
                account.setEmail(resultSet.getString("email"));
                account.setPassword(resultSet.getString("password"));
                accounts.add(account);
            }

        } catch (SQLException throwables) {
            System.out.println(throwables.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        }
        return accounts;
    }
    public List<Account> getAllAccount() {
        List<Account> accounts = new ArrayList<>();
        String sql = "select * from jdbc.Account";

        try {
            Connection connection = JdbcUtils.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Account account = new Account();
                int accountId = resultSet.getInt("account_id");
                account.setAccountId(accountId);
                account.setFullName(resultSet.getString("full_name"));
                account.setEmail(resultSet.getString("email"));
                account.setPassword(resultSet.getString("password"));

                accounts.add(account);
            }

        } catch (SQLException throwables) {
            System.out.println(throwables.getMessage());
        }finally {
            JdbcUtils.closeConnection();
        }
        return accounts;
    }
    public boolean loginAccount(String email, String password) {

        String sql = "select * from jdbc.account where email = ? and password = ?";
        Connection connection = JdbcUtils.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException throwables) {
            System.out.println(throwables.getMessage());
            return false;
        } finally {
            JdbcUtils.closeConnection();
        }
    }
}
