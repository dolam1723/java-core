package com.vti.backend.repository;

import com.mysql.cj.jdbc.JdbcConnection;
import com.vti.entity.Account;
import com.vti.entity.Department;
import com.vti.utils.JdbcUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DepartmentRepository {
    public List<Department> getDepartments(){
        String sql = "Select * from department";
        Connection connection = JdbcUtils.getConnection();
        List<Department> departmentList = new ArrayList<>();

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                Department department = new Department();
                department.setDepartmentId(resultSet.getInt("department_id"));
                department.setDepartmentName(resultSet.getString("department_name"));
                departmentList.add(department);
            }
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            JdbcUtils.closeConnection();
        }
        return departmentList;
    }
    public List<Department> getDepartmentsById(int id){
        String sql = "Select * from department where department_id = ?";
        Connection connection = JdbcUtils.getConnection();
        List<Department> departmentList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Department department = new Department();
                int departmentId = resultSet.getInt("department_id");
                department.setDepartmentId(departmentId);
                department.setDepartmentName(resultSet.getString("department_name"));
                departmentList.add(department);
            }
        } catch (SQLException throwables) {
            System.out.println(throwables.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        }
        return departmentList;
    }
    public void createDepartment(String name) {
        String sql = "INSERT INTO jdbc.department (department_name) Value(?)";
        Connection connection = JdbcUtils.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, name);
            int resultSet = preparedStatement.executeUpdate();
            if (resultSet == 0) {
                System.out.println("Failed");
            } else {
                System.out.println("Success");
            }
            JdbcUtils.closeConnection();
        } catch (SQLException throwables) {
            System.out.println(throwables.getMessage());
        }
    }
    public boolean isDepartmentNameExists(String name){
        String sql = "call checkDepartmentName(?)";
        Connection connection = JdbcUtils.getConnection();
        try {
//            PreparedStatement preparedStatement = connection.prepareStatement(sql);
//            preparedStatement.setString(1, name);
//            ResultSet resultSet = preparedStatement.executeQuery();
                CallableStatement callableStatement = connection.prepareCall(sql);
                ResultSet resultSet = callableStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException throwables) {
            System.out.println(throwables.getMessage());
            return false;
        } finally {
            JdbcUtils.closeConnection();
        }
    }
    public boolean isDepartmentIdExists(int id){
        String sql = "call checkDepartmentId(?)";
        Connection connection = JdbcUtils.getConnection();
        try {
//            PreparedStatement preparedStatement = connection.prepareStatement(sql);
//            preparedStatement.setString(1, name);
//            ResultSet resultSet = preparedStatement.executeQuery();
            CallableStatement callableStatement = connection.prepareCall(sql);
            ResultSet resultSet = callableStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException throwables) {
            System.out.println(throwables.getMessage());
            return false;
        } finally {
            JdbcUtils.closeConnection();
        }
    }
    public void updateDepartment(int id, String oldName, String newName) {
        String sql = "UPDATE jdbc.account set department_name = ? where department_id = ? and department_name = ?";
        Connection connection = JdbcUtils.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, newName);
            preparedStatement.setInt(2, id);
            preparedStatement.setString(3, oldName);
            int rs = preparedStatement.executeUpdate();
            if (rs == 0) {
                System.out.println("Failed");
            } else {
                System.out.println("Success");
            }

        } catch (SQLException throwables) {
            System.out.println(throwables.getMessage());
        } finally {
            JdbcUtils.closeConnection();
        }
    }
}
