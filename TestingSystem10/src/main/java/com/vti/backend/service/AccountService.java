package com.vti.backend.service;

import com.vti.backend.repository.AccountRepository;
import com.vti.entity.Account;

import java.util.List;

public class AccountService implements IAccountService {
    private AccountRepository repository = new AccountRepository();

    @Override
    public void createAccount(String username, String email, String password) {
        repository.createAccount(username, email, password);
    }

    @Override
    public void updateAccount(int id, String oldPassword, String newPassword) {
        repository.updateAccount(id, oldPassword, newPassword);
    }

    @Override
    public void deleteAccount(int id) {
        repository.deleteAccount(id);
    }

    @Override
    public List<Account> findByEmail(String emailtim) {
        return repository.findByEmail(emailtim);
    }

    @Override
    public List<Account> getAllAccount() {
        return repository.getAllAccount();
    }

    @Override
    public boolean loginAccount(String email, String password) {
        return repository.loginAccount(email, password);
    }

}
