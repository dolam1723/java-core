package com.vti.backend.service;

import com.vti.backend.repository.DepartmentRepository;
import com.vti.entity.Department;

import java.util.List;

public class DepartmentService implements IDepartmentService {
    private DepartmentRepository repository = new DepartmentRepository();

    @Override
    public void createDepartment(String name) {
        repository.createDepartment(name);
    }

    @Override
    public List<Department> getDepartments() {
        return repository.getDepartments();
    }

    @Override
    public List<Department> getDepartmentsById(int id) {
        return repository.getDepartmentsById(id);
    }

    @Override
    public boolean isDepartmentNameExists(String name) {
        return repository.isDepartmentNameExists(name);
    }

    @Override
    public boolean isDepartmentIdExists(int id) {
        return repository.isDepartmentIdExists(id);
    }

    @Override
    public void updateDepartment(int id, String oldName, String newName) {

    }
}
