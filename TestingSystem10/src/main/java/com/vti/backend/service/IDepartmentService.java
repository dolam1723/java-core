package com.vti.backend.service;

import com.vti.entity.Department;

import java.util.List;

public interface IDepartmentService {
    void createDepartment(String name);
    List<Department> getDepartments();
    List<Department> getDepartmentsById(int id);
boolean isDepartmentNameExists(String name);
boolean isDepartmentIdExists(int id);
void updateDepartment(int id,String oldName,String newName);
}
