package com.vti.utils;

import java.util.Scanner;

public class ScannerUtils {

    static Scanner scanner = new Scanner(System.in);

    public static String inputString() {
        return scanner.nextLine();
    }

    public static String inputEmail() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Moi b nhap vao email ");
        String email = scanner.nextLine();
        while (!email.contains("@")) {
            System.out.println("Email khong dung dinh dang, moi b nhap lai: ");
            email = scanner.nextLine();
        }
        return email;
    }

    public static int inputNumber(int min, int max) {
        int number = 0;
        while (true) {
            try {
                number = Integer.parseInt(scanner.nextLine());
                if (number < min || number > max) {
                    System.out.println("Số ko đúng định dạng, mời bạn nhập lại");
                    continue;
                }
                break;
            } catch (NumberFormatException ex) {
                System.err.println("Nhập vào phải là số, mời nhập lại!");
            }
        }
        return number;
    }


    public static String inputPassword() {
        System.out.println("Mời bạn nhập vào password");
        String password = scanner.nextLine();
        while (password.length() < 6 || password.length() > 12) {
            System.out.println("password ko đúng định dạng, mời bạn nhập lại");
            password = scanner.nextLine();
        }
        return password;
    }

    public static int inputNumber() {
        int number = 0;
        while (true) {
            try {
                number = Integer.parseInt(scanner.nextLine());
                if (number <= 0) {
                    System.out.println("Số phải lớn hơn 0, mời nhập lại");
                    continue;
                }
                break;
            } catch (NumberFormatException ex) {
                System.err.println("Nhập vào phải là số, mời nhập lại!");
            }
        }
        return number;
    }

}
