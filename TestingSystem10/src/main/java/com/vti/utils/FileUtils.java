package com.vti.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class FileUtils {
    public static boolean checkFileExists(String pathFile) {
        File file = new File(pathFile);
        if (file.exists()) {
            System.out.println("File có tồn tại!");
            return true;
        } else {
            System.out.println("File không tồn tại! ");
            return false;
        }
    }

    public static void createFile(String name) {
        String pathFile = "C:\\Users\\Admin\\OneDrive\\Documents\\GitHub\\java-core\\TestingSystem6-7\\data\\" + name;
        File file = new File(pathFile);
        try {
            if (file.createNewFile()) {
                System.out.println("Tao file thanh cong!");
            } else {
                System.out.println("Tao file that bai");
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void deleteFile(String path) {
        File file = new File(path);
        if (file.exists()) {
            System.out.println(file.delete() ? "Xoa thanh cong!" : "Xoa that bai");
        } else {
            System.out.println("File k ton tai");
        }
    }

    public static void checkFileOrFolder(String path) {
        File file = new File(path);
        if (file.isDirectory()) {
            System.out.println("path is Folder");
        } else {
            System.out.println(file.exists() ? "path is file" : "path is not exists");
        }
    }

    public static void getAllFile(String folderPath){
        File file = new File(folderPath);
        if (file.isDirectory()){
            for (String fileName : file.list()){
                System.out.println(fileName);
            }

        }else {
            System.out.println("Duong dan k hop le! ");
        }
    }

    public static void renameFile(String oldName,String newName){
        File file = new File(oldName);
        File file2 = new File(newName);
        if (file2.exists()){
            System.out.println("K duoc dat ten file exists");
        } boolean success = file.renameTo(file2);
        System.out.println(success ? "Thay doi ten thanh cong " : "Thay doi ten that bai");
    }

    public static void readFile(String path) throws IOException {
        if (checkFileExists(path)){
            FileInputStream fileInputStream = new FileInputStream(path);
            byte[] b = new byte[1024];
            int length = fileInputStream.read(b);
            while(length > -1){
                String content = new String (b, 0,length);
                System.out.println(content);
                System.out.println("--");
                length = fileInputStream.read(b);
            }
            fileInputStream.close();
        }
    }
    public static void writeFile(String path) throws IOException{
        if (checkFileExists(path)){
            Scanner scanner = new Scanner(System.in);
            System.out.println("Viết đi e : ");
            String content = scanner.nextLine();
            FileOutputStream fileOutputStream = new FileOutputStream(path, true);
            fileOutputStream.write(System.getProperty("line.separator").getBytes());

            fileOutputStream.write(content.getBytes());
            fileOutputStream.close();
        }
    }
}
