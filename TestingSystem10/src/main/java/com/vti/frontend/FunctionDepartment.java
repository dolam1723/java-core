package com.vti.frontend;

import com.vti.backend.controller.DepartmentController;
import com.vti.entity.Department;
import com.vti.utils.ScannerUtils;

import java.util.List;

public class FunctionDepartment {
    DepartmentController departmentController = new DepartmentController();

    public void getDepartments() {
        List<Department> departmentList = departmentController.getDepartments();
        String leftAlignFormat = "| %-3s| %-19s | %n";
        System.out.format("+----+---------------------+%n");
        System.out.format("| id |    departmentname   |%n");
        System.out.format("+----+---------------------+%n");
        for (Department department : departmentList) {
            System.out.format(leftAlignFormat, department.getDepartmentId(), department.getDepartmentName());
        }
        System.out.format("+----+-----------------+%n");

    }

    public void getDepartmentsById() {
        List<Department> departmentList = departmentController.getDepartmentsById(5);
        String leftAlignFormat = "| %-3s| %-19s | %n";
        System.out.format("+----+---------------------+%n");
        System.out.format("| id |    departmentname   |%n");
        System.out.format("+----+---------------------+%n");
        for (Department department : departmentList) {
            System.out.format(leftAlignFormat, department.getDepartmentId(), department.getDepartmentName());
        }
        System.out.format("+----+-----------------+%n");

    }

    public void createDepartment() {
        System.out.println("Moi nhap ten phong ban: ");
        String name = ScannerUtils.inputString();
        while (true) {
            if (departmentController.isDepartmentNameExists(name)) {
                System.err.println("Phong ban da ton tai, vui long nhap lai: ");
                name = ScannerUtils.inputString();
            } else {
                break;
            }
        }
        departmentController.createDepartment(name);
        System.out.println("Them moi thanh cong");
    }

    public void updateDepartment() {
        System.out.println("NHap id department b muon thay ten: ");
        int id = ScannerUtils.inputNumber();
        while (true) {
            if (departmentController.isDepartmentIdExists(id)) {
                System.out.println("Nhap vao ten cu: ");
                String oldName = ScannerUtils.inputString();
                System.out.println("Nhap vao ten moi: ");
                String newName = ScannerUtils.inputString();
                if (departmentController.isDepartmentNameExists(newName)) {
                    System.out.println("Ten moi da bi trung, vui long nhap lai: ");
                } else {
                    System.out.println("Nhap lai ten moi: ");
                    newName = ScannerUtils.inputString();
                }
            } else {
                System.out.println("Khong tim thay phong ban co id =" + id);
            }

        }//departmentController.updateDepartment(id,newName,oldName);


    }
}

