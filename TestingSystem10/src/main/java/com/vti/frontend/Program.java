package com.vti.frontend;

import com.vti.backend.Example;
import com.vti.utils.ScannerUtils;

public class Program {
    public static void main(String[] args) {
        Function function = new Function();

        while (true) {
            System.out.println("-----".repeat(20));
            System.out.println("Moi b chon");
            System.out.println("1. them account");
            System.out.println("2. sua account");
            System.out.println("3. xoa account theo id");
            System.out.println("4. Tim kiem Account theo email");
            System.out.println("5. Tat ca account");
            System.out.println("6. Login vao he thong");
            System.out.println("7. Thoat");
            int chose = ScannerUtils.inputNumber(1, 7);
            switch (chose) {
                case 1:
                    function.createAccount();

                    break;
                case 2:
                    function.updateAccount();
                    break;
                case 3:

                    function.deleteAccount();
                    break;
                case 4:
                    function.findByEmail();
                    break;
                case 5:
                    function.getAllAccount();
                    break;
                case 6:
                    function.loginAccount();
                    break;
                case 7:
                    break;
            }
        }
    }
}
