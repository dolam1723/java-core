package com.vti.frontend;

import com.vti.utils.ScannerUtils;

import java.util.Scanner;

public class Program2 {
    public static void main(String[] args) {
        FunctionDepartment functionDepartment = new FunctionDepartment();

        System.out.println("Moi b dang nhap");
        System.out.println("Tai khoan: ");
        String tk = ScannerUtils.inputString();
        System.out.println("Mat khau: ");
        String mk = ScannerUtils.inputPassword();
        if (tk != "ADMIN" || tk != "USER") {
            while (tk == "ADMIN" || tk == "USER") {
                System.out.println("b da nhap sai tai khoan, vui long nhap lai: ");
                tk = ScannerUtils.inputString();
            }
        } else if ( mk != "123456") {
            System.out.println("b da nhap sai mat khau, vui long nhap lai: ");
            mk = ScannerUtils.inputPassword();
        } else if (tk == "ADMIN" && mk == "123456") {
            while (true) {
                System.out.println("-----".repeat(20));
                System.out.println("User menu");
                System.out.println("1. hien thi danh sach");
                System.out.println("2. tim kiem theo id");
                System.out.println("3. tim kiem theo usename va email");
                System.out.println("4. hien thi danh sach tat ca department");
                System.out.println("5. tim kiem department theo id");
                System.out.println("6. tim kiem department theo department name");
                System.out.println("7. Thoat");
                int chose = ScannerUtils.inputNumber(1, 7);
                switch (chose) {
                    case 1:
                        functionDepartment.createDepartment();
                        break;
                    case 2:
                        functionDepartment.updateDepartment();
                        break;
                    case 3:
                        //functionDepartment.deleteAccount();
                        break;
                    case 4:
                        functionDepartment.getDepartmentsById();
                        break;
                    case 5:
                        functionDepartment.getDepartments();
                        break;
                    case 6:
                        //functionDepartment.loginAccount();
                        break;
                    case 7:
                        break;
                }
            }
        } else if (tk == "USER" && mk == "123456") {
            while (true) {
                System.out.println("-----".repeat(20));
                System.out.println("User menu");
                System.out.println("1. hien thi danh sach");
                System.out.println("2. tim kiem theo id");
                System.out.println("3. tim kiem theo usename va email");
                System.out.println("4. hien thi danh sach tat ca department");
                System.out.println("5. tim kiem department theo id");
                System.out.println("6. tim kiem department theo department name");
                System.out.println("7. Thoat");
                int chose = ScannerUtils.inputNumber(1, 7);
                switch (chose) {
                    case 1:
                        functionDepartment.createDepartment();
                        break;
                    case 2:
                        functionDepartment.updateDepartment();
                        break;
                    case 3:
                        //functionDepartment.deleteAccount();
                        break;
                    case 4:
                        functionDepartment.getDepartmentsById();
                        break;
                    case 5:
                        functionDepartment.getDepartments();
                        break;
                    case 6:
                        //functionDepartment.loginAccount();
                        break;
                    case 7:
                        break;
                }
            }
        }

    }
}
