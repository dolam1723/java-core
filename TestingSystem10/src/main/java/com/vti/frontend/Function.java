package com.vti.frontend;

import com.vti.backend.Example;
import com.vti.backend.controller.AccountController;
import com.vti.entity.Account;
import com.vti.utils.ScannerUtils;

import java.util.List;

public class Function {
    AccountController accountController = new AccountController();

    public void createAccount() {

        System.out.println("Nhap ten");
        String username = ScannerUtils.inputString();

        String email = ScannerUtils.inputEmail();

        String password = ScannerUtils.inputPassword();
        accountController.createAccount(username, email, password);
    }

    public void updateAccount() {
        System.out.println("NHap id nguoi dung muon thay password: ");
        int id = ScannerUtils.inputNumber();
        System.out.println("Nhap vao password cu: ");
        String oldpassword = ScannerUtils.inputPassword();
        System.out.println("Nhap vao password moi: ");
        String newPassword = ScannerUtils.inputPassword();
accountController.updateAccount(id,oldpassword,newPassword);
    }

    public void deleteAccount() {
        System.out.println("NHap id nguoi dung xoa: ");
        int id = ScannerUtils.inputNumber();
        accountController.deleteAccount(id);
    }

    public void findByEmail() {
        System.out.println("Nhap email b muon tim: ");
        String emailtim = ScannerUtils.inputString();
        List<Account> accounts = accountController.findByEmail(emailtim);
        String leftAlignFormat = "| %-3s| %-15s | %-19s | %-19s | %n";
        System.out.format("+----+-----------------+---------------------+---------------------+%n");
        System.out.format("| id |      fullName   |      email          |     password        |%n");
        System.out.format("+----+-----------------+---------------------+---------------------+%n");
        for (Account account : accounts){
            System.out.format(leftAlignFormat,account.getAccountId(),account.getFullName(),account.getEmail(),account.getPassword());
        }
        System.out.format("+----+-----------------+---------------------+---------------------+%n");

    }

    public void getAllAccount() {
        List<Account> accounts = accountController.getAllAccount();
        String leftAlignFormat = "| %-3s| %-15s | %-19s | %-19s | %n";
        System.out.format("+----+-----------------+---------------------+---------------------+%n");
        System.out.format("| id |      fullName   |      email          |     password        |%n");
        System.out.format("+----+-----------------+---------------------+---------------------+%n");
        for (Account account : accounts){
            System.out.format(leftAlignFormat,account.getAccountId(),account.getFullName(),account.getEmail(),account.getPassword());
        }
        System.out.format("+----+-----------------+---------------------+---------------------+%n");

    }

    public void loginAccount() {
        System.out.println("Nhap email cua b: ");
String email = ScannerUtils.inputEmail();
        System.out.println("Nhap password cua b: ");
        String password = ScannerUtils.inputPassword();
        if(accountController.loginAccount(email,password) == true){
            System.out.println("Dang nhap thanh cong");
        } else {
            System.out.println("Dang nhap that bai");
        }

    }
}
