# DROP DATABASE IF EXISTS jdbc;
# CREATE DATABASE jdbc;
# use jdbc;
# drop table if exists Account;
# create table Account (
#                          account_id			int auto_increment primary key,
#                          full_name			varchar(50),
#                          email				varchar(50) not null,
#                          password			varchar(50) not null
# );
# drop table if exists Department;
# create table Department (
#                          department_id			int auto_increment primary key,
#                          department_name			varchar(50) not null unique
# );
use jdbc;
DROP PROCEDURE IF EXISTS checkDepartmentName;
Delimiter $$
create procedure checkDepartmentName (IN va_department_name nvarchar(20))
begin
    select * from jdbc.department where department_name = va_department_name;
end $$
delimiter ;
DROP PROCEDURE IF EXISTS checkDepartmentId;
Delimiter $$
create procedure checkDepartmentId (IN va_department_Id int)
begin
    select * from jdbc.department where department_id = va_department_Id;
end $$
delimiter ;

call checkDepartmentId(1);