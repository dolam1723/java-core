DROP DATABASE IF EXISTS assignment10;
create database assignment10;
use assignment10;

drop table if exists Department;
create table `Department`
(
    department_id   int auto_increment primary key,
    department_name varchar(50) not null unique
);

create table `User`
(
    id            int primary key auto_increment,
    `role`        enum ('ADMIN', 'USER') not null,
    user_name     nvarchar(20)           not null unique,
    `password`    nvarchar(20)           not null,
    email         varchar(30)            not null unique,
    date_of_birth date,
    department_id int,
    foreign key (department_id) REFERENCES Department (department_id)
);

INSERT INTO `assignment10`.`Department` (`department_name`) values ('Java');
INSERT INTO `assignment10`.`Department` (`department_name`) values ('php');
INSERT INTO `assignment10`.`Department` (`department_name`) values ('scrum master');

insert into `assignment10`.`User` (`role`,user_name,password,email,date_of_birth,department_id)
values ('ADMIN','ADMIN','123456','admin@gmail.com','2004-03-22',1);
insert into `assignment10`.`User` (`role`,user_name,password,email,date_of_birth,department_id)
values ('USER','USER','123456','user@gmail.com','2005-04-26',2);