import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Exercise3 {
    public static void main(String[] args) {
        Exercise3 exercise3 = new Exercise3();
        exercise3.question1();
        exercise3.question2();
        exercise3.question3();
        exercise3.question4();
        exercise3.question5();
    }

    public void question1() {
        Date date = new Date();
        Locale locale = new Locale("vi", "VN");
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.DEFAULT, locale);
        String dateString = dateFormat.format(date);
        System.out.println(dateString);
    }

    public void question2() {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        String dateString = dateFormat.format(date);
        System.out.println(dateString);
    }
    public void question3(){
        Date date= new Date();
        String pattern = "yyyy";
        DateFormat dateFormat = new SimpleDateFormat(pattern);
        String dateString = dateFormat.format(date);
        System.out.println(dateString);
    }
    public void question4(){
        Date date= new Date();
        String pattern = "yyyy-MM";
        DateFormat dateFormat = new SimpleDateFormat(pattern);
        String dateString = dateFormat.format(date);
        System.out.println(dateString);
    }
    public void question5(){
        Date date= new Date();
        String pattern = "MM-dd";
        DateFormat dateFormat = new SimpleDateFormat(pattern);
        String dateString = dateFormat.format(date);
        System.out.println(dateString);
    }

}

