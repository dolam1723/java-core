import java.util.Scanner;

public class Exercise5 {
    public static void main(String[] args) {
        Exercise5 exercise5 = new Exercise5();
        exercise5.question7();
    }
    public void question5(){

    }
    public void question6() {
        Scanner scanner = new Scanner(System.in);
        Department department = new Department();
        System.out.println("Nhap vao id cua department: ");
        department.departmentId = scanner.nextInt();

        System.out.println("Nhap vao ten cua department: ");
        department.departmentName = scanner.next();

        System.out.println("Thông tin Department vừa nhập: \nID: " + department.departmentId + "\n Name: " + department.departmentName);
    }

    public void question7() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhap vao so chan: ");
        int a = scanner.nextInt();
        while (a % 2 != 0){
            System.out.println("Nhap lai so chan: ");
            a = scanner.nextInt();
        }
        System.out.println("So chan ban vua nhap la: " + a);
    }
    public void question8(){
        Scanner scanner = new Scanner(System.in);
        int c;
        while (true) {
            System.out.println("Mời bạn chọn chức năng: 1. Tạo Account,2. Tạo Department");

            c = scanner.nextInt();
            if (c == 1 || c == 2) {
                switch (c) {
                    case 1:
                        question5();
                        break;
                    case 2:
                        question7();
                        break;
                }
                return;
            } else {
                System.out.println("Nhập lại: ");
            }
        }
    }

}
