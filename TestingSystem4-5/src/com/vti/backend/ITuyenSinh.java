package com.vti.backend;

import com.vti.entity.Student3;

public interface ITuyenSinh {
    void addNewStudent();

    void viewStudent();

    Student3 findByStudentCode(String studentCode);
}
