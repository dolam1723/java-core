package com.vti.entity;

import java.text.DateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;

public class Program {
    public static void main(String[] args) {
        System.out.println("Khoi tao doi tuong");

        Department department1 = new Department();
        department1.departmentId = 1;
        department1.departmentName = "Phong ban 1";

        Department department2 = new Department();
        department2.departmentId = 2;
        department2.departmentName = "Phong ban 2";

        Department department3 = new Department();
        department3.departmentId = 3;
        department3.departmentName = "Phong ban 3";


        Position position1 = new Position();
        position1.positionId = 1;
        position1.positionName = PositionName.TEST;

        Position position2 = new Position();
        position2.positionId = 2;
        position2.positionName = PositionName.DEV;

        Position position3 = new Position();
        position3.positionId = 3;
        position3.positionName = PositionName.SCRUMMASTER;

        Group group1 = new Group();
        group1.groupID = 1;
        group1.groupName = "group 1";
        group1.creatorId = 1111;
        group1.createDate = LocalDate.now();

        Group group2 = new Group();
        group2.groupID = 2;
        group2.groupName = "group 2";
        group2.creatorId = 2222;
        group2.createDate = LocalDate.now();

        Group group3 = new Group();
        group3.groupID = 3;
        group3.groupName = "group 3";
        group3.creatorId = 3333;
        group3.createDate = LocalDate.now();

        Group group4 = new Group();
        group4.groupID = 4;
        group4.groupName = "group 4";
        group4.creatorId = 4444;
        group4.createDate = LocalDate.now();


        Account account1 = new Account();
        account1.accountId = 1;
        account1.email = "diasodw@gmail.com";
        account1.userName = "asdjasddas";
        account1.fullName = "Nguyen Van A";
        account1.department = department1;
        account1.position = position1;
        account1.createDate = LocalDate.now();
        account1.groups = new Group[]{group1, group2};

        Account account2 = new Account();
        account2.accountId = 2;
        account2.email = "odkfiosdf@gmail.com";
        account2.userName = "hdgrfhgfh";
        account2.fullName = "Nguyen Van B";
        account2.department = department2;
        account2.position = position2;
        account2.createDate = LocalDate.now();
        account2.groups = new Group[]{group2, group3, group4};


        Account account3 = new Account();
        account3.accountId = 3;
        account3.email = "hdfgreg@gmail.com";
        account3.userName = "gdsgsfsdfs";
        account3.fullName = "Nguyen Van C";
        account3.department = department3;
        account3.position = position3;
        account3.createDate = LocalDate.now();
        account3.groups = new Group[]{group1};

// khởi tạo danh sách Account
        Account[] arraccount1 = {account1, account2, account3};
        group1.accounts = arraccount1;
// khởi tạo danh sách department
        Department[] arrdepartment1 = {department1, department2, department3};

        //---------------------------- assignment 1 ----------------------------

        System.out.println("ID tai khoan 1 " + account1.accountId);
        System.out.println("email tai khoan 2 " + account2.position.positionName);
        System.out.println("phong ban tai khoan " + account3.department.departmentName);

        //---------------------------- assignment 2 ----------------------------

        System.out.println("-----------Question 1-----------");
        if (account2.department == null) {
            System.out.println("Nhan vien nay chua co phong ban");
        } else System.out.println("Phong ban cua nhan vien nay la " + account2.department.departmentName);

        System.out.println("-----------Question 2-----------");
        if (account2.groups.length < 3) {
            System.out.println("Group cua nhan vien nay la Java Fresher, C# Fresher");
        } else if (account2.groups.length == 3) {
            System.out.println("Nhan vien nay la nguoi quan trong, tham gia nhieu group");
        } else if (account2.groups.length == 4) {
            System.out.println("Nhan vien nay la nguoi hong chuyen, tham gia tat ca cac group");
        } else System.out.println("Cac truong hop con lai");

        System.out.println("-----------Question 3-----------");
        String ques3;
        ques3 = (account2.department == null) ? "Nhan vien nay chua co phong ban" : "Phong ban cua nhan vien nay la " + account2.department.departmentName;
        System.out.println(ques3);

        System.out.println("-----------Question 4-----------");
        String ques4;
        ques4 = (account1.position.positionName.toString() == "DEV") ? "Day la Developer" : "Nguoi nay khong phai la Developer";
        System.out.println(ques4);
        System.out.println("-----------Question 5-----------");
        switch (group1.accounts.length) {
            case 1:
                System.out.println("Nhom co 1 thanh vien");
                break;
            case 2:
                System.out.println("Nhom co 2 thanh vien");
                break;
            case 3:
                System.out.println("Nhom co 3 thanh vien");
                break;
            default:
                System.out.println("Nhom co nhieu thanh vien");
        }

        System.out.println("-----------Question 6-----------");
        switch (account2.groups.length) {
            case 0:
                System.out.println("Nhan vien nay chua co group");
                break;
            case 1:
            case 2:
                System.out.println("Group cua nhan vien nay la Java Fresher, C# Fresher");
                break;
            case 3:
                System.out.println("Nhan vien nay la nguoi quan trong, tham gia nhieu group");
                break;
            case 4:
                System.out.println("Nhan vien nay la nguoi hong chuyen, tham gia tat ca cac group");
                break;
        }
        System.out.println("-----------Question 7-----------");
        switch (account1.position.positionName.toString()) {
            case "DEV":
                System.out.println("Day la Developer");
                break;
            default:
                System.out.println("Nguoi nay khong phai Developer");

        }

        System.out.println("-----------Question 8-----------");
        for (Account account : arraccount1) {
            System.out.println("\nEmail :" + account.email + " \nFullName :" + account.fullName + "\nPhong ban :" + account.department.departmentName);
        }

        System.out.println("-----------Question 9-----------");
        for (Department department : arrdepartment1) {
            System.out.println("\nID :" + department.departmentId + "\nName :" + department.departmentName);
        }

        System.out.println("-----------Question 10-----------");
        for (int i = 0; i < arraccount1.length; i++) {
            System.out.println("\nThong tin account thu " + (i + 1) + " la :");
            System.out.println("Email :" + arraccount1[i].email + " \nFullName :" + arraccount1[i].fullName + "\nPhong ban :" + arraccount1[i].department.departmentName);
        }

        System.out.println("-----------Question 11-----------");
        for (int i = 0; i < arrdepartment1.length; i++) {
            System.out.println("\nThong tin department thu " + (i + 1) + " la :");
            System.out.println("ID :" + arrdepartment1[i].departmentId + "\nName :" + arrdepartment1[i].departmentName);
        }

        System.out.println("-----------Question 12-----------");
        System.out.println("\nThong tin 2 account dau tien la :");
        for (int i = 0; i < 2; i++) {

            System.out.println("\nEmail :" + arraccount1[i].email + " \nFullName :" + arraccount1[i].fullName + "\nPhong ban :" + arraccount1[i].department.departmentName);
        }
        System.out.println("-----------Question 13-----------");
        System.out.println("\nTat ca thong tin account tru account thu 2 la :");
        for (int i = 0; i < arraccount1.length; i++) {
            if (i == 1) {
                continue;
            }
            System.out.println("\nThong tin account thu " + (i + 1) + " la :");
            System.out.println("Email :" + arraccount1[i].email + " \nFullName :" + arraccount1[i].fullName + "\nPhong ban :" + arraccount1[i].department.departmentName);
        }
        System.out.println("-----------Question 14-----------");
        System.out.println("\nTat ca thong tin account co id < 4 la :");
        int Dem = 0;
        for (int i = 0; i < arraccount1.length; i++) {
            if (i < 4) {
                System.out.println("\nThong tin account thu " + (i + 1) + " la :");
                System.out.println("Email :" + arraccount1[i].email + " \nFullName :" + arraccount1[i].fullName + "\nPhong ban :" + arraccount1[i].department.departmentName);
                Dem++;
            }
            if (Dem == 0) {
                System.out.println("Khong co account nao co id < 4");
            }
        }

        System.out.println("-----------Question 15-----------");
        for (int i = 0; i <= 20; i++) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }

        System.out.println("-----------Question 16-----------");

        System.out.println("10");
        System.out.println("Thong tin cac account la: ");
        int number = 0;
        while (number < arraccount1.length) {
            System.out.println("\nThong tin account thu " + (number + 1) + " la: ");
            System.out.println("Email :" + arraccount1[number].email + " \nFullName :" + arraccount1[number].fullName + "\nPhong ban :" + arraccount1[number].department.departmentName);
            number++;
        }

        System.out.println("11");
        System.out.println("Thong tin cac phong ban la: ");
        int number1 = 0;
        while (number1 < arrdepartment1.length) {
            System.out.println("\nThong tin department thu " + (number1 + 1) + " la: ");
            System.out.println("ID: " + arrdepartment1[number1].departmentId + "\nName: " + arrdepartment1[number1].departmentName);
            number1++;
        }
        System.out.println("12");
        System.out.println("Thong tin 2 department dau tien la: ");
        int number2 = 0;
        while (number2 < arrdepartment1.length) {
            System.out.println("\nThong tin department thu " + (number2 + 1) + " la: ");
            System.out.println("ID :" + arrdepartment1[number2].departmentId + "\nName :" + arrdepartment1[number2].departmentName);
            number2++;
        }

        System.out.println("13");
        System.out.println("Tat ca cac account tru account thu 2 la: ");
        int number3 = 0;
        while (number3 < arraccount1.length){

            if (number3 == 1) {
                number3++;
                continue;
            } else
                System.out.println("\nThong tin account thu " + (number3 + 1) + " la: ");
            System.out.println("Email :" + arraccount1[number3].email + " \nFullName :" + arraccount1[number3].fullName + "\nPhong ban :" + arraccount1[number3].department.departmentName);
            number3++;
        }

        System.out.println("14");
        System.out.println("Account co ID < 4 la : ");
        int number4 = 0;
        while (number4 < arraccount1.length) {
            if (arraccount1[number4].accountId < 4) {
                System.out.println("\nThong tin account thu " + (number4 + 1) + " la: ");
                System.out.println("Email :" + arraccount1[number4].email + " \nFullName :" + arraccount1[number4].fullName + "\nPhong ban :" + arraccount1[number4].department.departmentName);
                number4++;
            } else
                number4++;
            continue;
        }
        System.out.println("15");
        System.out.println("So chan <= 20");
        int number5 = 0;
        while (number5 <= 20) {
            if (number5 % 2 == 0) {
                System.out.println(number5);
                number5++;
                continue;
            }
            number5++;
        }
        System.out.println("-----------Question 17-----------");
        System.out.println("10");
        System.out.println("Thong tin cac account la: ");
        int number6 = 0;
        do {
            System.out.println("\nThong tin account thu " + (number6 + 1) + " la: ");
            System.out.println("Email :" + arraccount1[number6].email + " \nFullName :" + arraccount1[number6].fullName + "\nPhong ban :" + arraccount1[number6].department.departmentName);
            number6++;
        }while (number6 < arraccount1.length);

        System.out.println("11");
        System.out.println("Thong tin cac phong ban la: ");
        int number7 = 0;
        do {
            System.out.println("\nThong tin department thu " + (number7 + 1) + " la: ");
            System.out.println("ID: " + arrdepartment1[number7].departmentId + "\nName: " + arrdepartment1[number7].departmentName);
            number7++;
        }while (number7 < arrdepartment1.length);
        System.out.println("12");
        System.out.println("Thong tin 2 department dau tien la: ");
        int number8 = 0;
        do {
            System.out.println("\nThong tin department thu " + (number8 + 1) + " la: ");
            System.out.println("ID :" + arrdepartment1[number8].departmentId + "\nName :" + arrdepartment1[number8].departmentName);
            number8++;
        }while (number8 < arrdepartment1.length);

        System.out.println("13");
        System.out.println("Tat ca cac account tru account thu 2 la: ");
        int number9 = 0;
        do {

            if (number9 == 1) {
                number9++;
                continue;
            } else
                System.out.println("\nThong tin account thu " + (number9 + 1) + " la: ");
            System.out.println("Email :" + arraccount1[number9].email + " \nFullName :" + arraccount1[number9].fullName + "\nPhong ban :" + arraccount1[number9].department.departmentName);
            number9++;
        } while (number9 < arraccount1.length);

        System.out.println("14");
        System.out.println("Account co ID < 4 la : ");
        int number10 = 0;
        do {
            if (arraccount1[number10].accountId < 4) {
                System.out.println("\nThong tin account thu " + (number10 + 1) + " la: ");
                System.out.println("Email :" + arraccount1[number10].email + " \nFullName :" + arraccount1[number10].fullName + "\nPhong ban :" + arraccount1[number10].department.departmentName);
                number10++;
            } else
                number10++;
            continue;
        }while (number10 < arraccount1.length);
        System.out.println("15");
        System.out.println("So chan <= 20");
        int number11 = 0;
        do {
            if (number11 % 2 == 0) {
                System.out.println(number11);
                number11++;
                continue;
            }
            number11++;
        }while (number11 <= 20);

        // Exercise 3 : Date Format
        System.out.println("-----------Exercise 3 : Date Format-----------");
        System.out.println("-----------Question 17-----------");

        /*Locale locale = new Locale("vi", "VN");
        DateFormat dateFormat1 = DateFormat.getDateInstance(DateFormat.DEFAULT, locale);
        String createDate = dateFormat1.format(exam1.createDate);
        System.out.println(exam1.createDate);*/

    }
}
