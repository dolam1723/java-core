package com.vti.entity;

import com.vti.Ultis.ScannerUltis;
import com.vti.backend.INews;

public class News implements INews {
    private int id;
    private String title;
    private String publishDate;
    private String Author;
    private String content;
    private float averageRate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public float getAverageRate() {
        return averageRate;
    }

    public void setAverageRate(float averageRate) {
        this.averageRate = averageRate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "News{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", publishDate='" + publishDate + '\'' +
                ", Author='" + Author + '\'' +
                ", content='" + content + '\'' +
                ", averageRate=" + averageRate +
                '}';
    }

    @Override
    public void Display() {
        System.out.println(this.title);
        System.out.println(this.publishDate);
        System.out.println(this.content);
        System.out.println(this.Author);
        System.out.println(this.id);

    }

    @Override
    public float Calculate() {
        System.out.println("Moi b danh gia");
        int[] rates = new int[3];
        rates[0] = ScannerUltis.inputNumber(1, 5);
        rates[1] = ScannerUltis.inputNumber(1, 5);
        rates[2] = ScannerUltis.inputNumber(1, 5);
        this.averageRate = (rates[0] + rates[1] + rates[2]) / 3;

        return averageRate;
    }

}
